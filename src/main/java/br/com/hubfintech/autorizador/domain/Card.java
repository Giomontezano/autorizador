package br.com.hubfintech.autorizador.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Card implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7834128313525786158L;

	@Id
	private String cardNumber;

	private Double availableAmount;

	@OneToMany(cascade = CascadeType.MERGE)
	@JsonManagedReference
	private List<Transaction> transactions;

	public Card() {

	}

	public Card(String cardNumber, Double availableAmount, List<Transaction> transactions) {
		super();
		this.cardNumber = cardNumber;
		this.availableAmount = availableAmount;
		this.transactions = transactions;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Double getAvailableAmount() {
		return availableAmount;
	}

	public void setAvailableAmount(Double availableAmount) {
		this.availableAmount = availableAmount;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

}
