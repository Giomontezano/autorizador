package br.com.hubfintech.autorizador.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Transaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private UUID transactionId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	private Card cardNumber;
	
	private String transactionType;
	
	private Date dateTime;
	
	private Double amount;

	public Transaction() {
		
	}
	
	public Transaction(UUID transactionId, Card cardNumber, String transactionType, Date dateTime, Double amount) {
		super();
		this.transactionId = transactionId;
		this.cardNumber = cardNumber;
		this.transactionType = transactionType;
		this.dateTime = dateTime;
		this.amount = amount;
	}

	public UUID getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(UUID transactionId) {
		this.transactionId = transactionId;
	}

	public Card getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(Card cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
