package br.com.hubfintech.autorizador.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CardResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7834128313525786158L;

	private String cardNumber;

	private String availableAmount;

	private List transactions;

	public CardResponse() {

	}

	public CardResponse(String cardNumber, String availableAmount, List transactions) {
		this.cardNumber = cardNumber;
		this.availableAmount = availableAmount;
		this.transactions = transactions;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getAvailableAmount() {
		return availableAmount;
	}

	public void setAvailableAmount(String availableAmount) {
		this.availableAmount = availableAmount;
	}

	public List getTransactions() {
		return transactions;
	}

	public void setTransactions(List transactions) {
		this.transactions = transactions;
	}
}
