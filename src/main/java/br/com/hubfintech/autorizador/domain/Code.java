package br.com.hubfintech.autorizador.domain;

public enum Code {

	APROVADO("00", "Aprovado"),
	SALDO_INSUFICIENTE("51", "Saldo Insuficiente"),
	CONTA_INVALIDA("14", "Conta Invalida"),
	ERRO_PROCESSAMENTO("96", "Erro de Processamento");
	
	private String codes;
	private String description;
	
	private Code(String codes, String description) {
		this.codes = codes;
		this.description = description;
	}

	public String getCodes() {
		return codes;
	}
	public void setCodes(String codes) {
		this.codes = codes;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
