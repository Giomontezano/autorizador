package br.com.hubfintech.autorizador.domain;

import java.io.Serializable;

public class Authorization implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8234122932496736875L;

	private String Action;
	
	private String CardNumber;

	private String amount;

//	private Double Amount;

	public String getAction() {
		return Action;
	}

	public void setAction(String action) {
		Action = action;
	}

	public String getCardNumber() {
		return CardNumber;
	}

	public void setCardNumber(String cardNumber) {
		CardNumber = cardNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
//	public Double getAmount() {
//		return Amount;
//	}

//	public void setAmount(Double amount) {
//		Amount = amount;
//	}

}
