package br.com.hubfintech.autorizador;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import br.com.hubfintech.autorizador.application.services.TransactionService;

@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
public class AutorizadorApplication {
	
	
	@Autowired
	private static TransactionService transactionService;

	
	public static void main(String[] args) throws IOException {
		SpringApplication.run(AutorizadorApplication.class, args);
		
		
		
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			ServerSocket ss = new ServerSocket(2002);
//			while (true) {
//				Socket s = ss.accept();
//				InputStream is = s.getInputStream();
//				BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
//			    String greeting = in.readLine();
//			    System.out.println(greeting);
//			    Authorization autorizacao = mapper.readValue(greeting, Authorization.class);
//			}
//			
//
//		} catch (Exception e) {
//			System.out.println(e);
//		}
		
//		try (ServerSocket serverSocket = new ServerSocket(2002)) {
//			 
//            System.out.println("Server is listening on port " + 2002);
// 
//            while (true) {
//                Socket socket = serverSocket.accept();
//                System.out.println("New client connected");
// 
//                InputStream input = socket.getInputStream();
//                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
//     
//                OutputStream output = socket.getOutputStream();
//                PrintWriter writer = new PrintWriter(output, true);
//                
//                do {
//                	String text = reader.readLine();
//                	ObjectMapper mapper = new ObjectMapper();
//                    Authorization autorizacao = mapper.readValue(text, Authorization.class);
//                    
//                    
////                    AuthorizationResponse response = transactionService.setTransaction(autorizacao);
//                    
//                    
////                    writer.println("Server: " + mapper.writeValueAsString(response));
//     
//                } while (!reader.equals("bye"));
//            }
// 
//        } catch (IOException ex) {
//            System.out.println("Server exception: " + ex.getMessage());
//            ex.printStackTrace();
//        }
//		
	}
	

}
