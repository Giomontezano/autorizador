package br.com.hubfintech.autorizador.adapter.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.hubfintech.autorizador.domain.Card;
import br.com.hubfintech.autorizador.application.services.CardService;

@RestController
@CrossOrigin
public class CardController {

	@Autowired
	private CardService cardService;

	@PostMapping("/card")
	@ResponseStatus(value = HttpStatus.CREATED)
	public Card cadastrarCartao(@RequestBody Card card) {
		return cardService.setCard(card);
	}
	
	@GetMapping("/card/{number}")
	public Card getACard(@PathVariable String number) {
		return cardService.getCard(number);
	}
	
}
