package br.com.hubfintech.autorizador.adapter.tcp;

import br.com.hubfintech.autorizador.application.useCase.AuthorizationUC;
import br.com.hubfintech.autorizador.domain.Authorization;
import br.com.hubfintech.autorizador.domain.AuthorizationResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.GenericMessage;

public class HeartbeatServer {

	@Autowired
	private AuthorizationUC authorizationUC;

	private final Logger log = LogManager.getLogger(HeartbeatServer.class);

	public Message<String> processRequest(byte[] payload, MessageHeaders messageHeaders) {
		ObjectMapper mapper = new ObjectMapper();
		AuthorizationResponse response;
		
		try {
			Authorization auth = mapper.readValue(new String(payload), Authorization.class);
			response = authorizationUC.process(auth);
			
			log.info("Transação respondida");
			return new GenericMessage<String>(mapper.writeValueAsString(response));
		} catch (Exception e) {
			log.error("Transação com erro");
			return null;
		}
	}
}