package br.com.hubfintech.autorizador.adapter.api;

import br.com.hubfintech.autorizador.application.services.TransactionService;
import br.com.hubfintech.autorizador.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class TransactionController {

	@Autowired
	private TransactionService transactionService;
	
	@GetMapping("/transaction/{transaction}")
	public Transaction getATransaction(@PathVariable UUID transaction) {
		return transactionService.getTransaction(transaction);
	}
	
	@GetMapping("/transactions")
	public Iterable<Transaction> getTransactions() {
		return transactionService.getTransactions();
	}

	@GetMapping("/transactions/{cardnumber}")
	public CardResponse getTransactionsByCard(@PathVariable String cardnumber) {
		return transactionService.getTransactionsByCard(cardnumber);
	}

	@PostMapping("/transaction")
	public AuthorizationResponse setATransaction(@RequestBody Authorization auth) {
		return transactionService.setTransaction(auth);
	}
	
	
}
