package br.com.hubfintech.autorizador.adapter.database;

import org.springframework.data.repository.CrudRepository;

import br.com.hubfintech.autorizador.domain.Card;

import java.util.Optional;

public interface CardRepository extends CrudRepository<Card, String> {

}
