package br.com.hubfintech.autorizador.adapter.database;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import br.com.hubfintech.autorizador.domain.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, UUID> {

}
