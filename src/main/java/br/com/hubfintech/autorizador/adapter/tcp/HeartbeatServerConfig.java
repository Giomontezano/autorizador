package br.com.hubfintech.autorizador.adapter.tcp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.ip.dsl.Tcp;
import org.springframework.integration.ip.tcp.connection.TcpNetServerConnectionFactory;

@Configuration
@EnableIntegration
public class HeartbeatServerConfig {

	@Bean
	public TcpNetServerConnectionFactory serverConnectionFactory() {
		TcpNetServerConnectionFactory connectionFactory = new TcpNetServerConnectionFactory(9999);
		return connectionFactory;
	}

	@Bean
	public IntegrationFlow heartbeatServerFlow(
			TcpNetServerConnectionFactory serverConnectionFactory,
			HeartbeatServer heartbeatServer) {
		return IntegrationFlows
				.from(Tcp.inboundGateway(serverConnectionFactory))
				.handle(heartbeatServer::processRequest)
				.get();
	}

	@Bean
	public HeartbeatServer heartbeatServer() {
		return new HeartbeatServer();
	}
}
