package br.com.hubfintech.autorizador.adapter.database.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import br.com.hubfintech.autorizador.adapter.database.CardRepository;
import br.com.hubfintech.autorizador.domain.Card;

@Component
public class DataLoader implements ApplicationRunner {

	@Autowired
    private CardRepository cardRepository;

    @Autowired
    public DataLoader(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    public void run(ApplicationArguments args) {
        cardRepository.save(new Card("1111111111111111", 1000.0, null));
        cardRepository.save(new Card("2222222222222222", 1000.0, null));
    }
}