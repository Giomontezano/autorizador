package br.com.hubfintech.autorizador.application.services;

import br.com.hubfintech.autorizador.adapter.database.CardRepository;
import br.com.hubfintech.autorizador.application.exception.InvalidAccountException;
import br.com.hubfintech.autorizador.domain.Card;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class CardService {

	@Autowired
	private CardRepository cardRepository;

	public Card getCard(String number) {
		Optional<Card> card = cardRepository.findById(number);

		if (card != null) {
			return card.get();
		}
		throw new InvalidAccountException();
	}

	
	public Iterable<Card> getCards() {
		return cardRepository.findAll();
	}
	
	public Card setCard(Card card) {
		return cardRepository.save(card);
	}
	
}
