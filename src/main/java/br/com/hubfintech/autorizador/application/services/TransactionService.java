package br.com.hubfintech.autorizador.application.services;

import br.com.hubfintech.autorizador.adapter.database.TransactionRepository;
import br.com.hubfintech.autorizador.application.useCase.AuthorizationUC;
import br.com.hubfintech.autorizador.domain.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.util.*;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private CardService cardService;

    @Autowired
    private AuthorizationUC authorizationUC;

    public Transaction getTransaction(UUID transaction) {
        Optional<Transaction> tran = transactionRepository.findById(transaction);

        if (tran != null) {
            return tran.get();
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "cartão não encontrado");
    }

    public Iterable<Transaction> getTransactions() {
        return transactionRepository.findAll();
    }

    public CardResponse getTransactionsByCard(String cardNumber) {
        Card card = cardService.getCard(cardNumber);
        CardResponse cardResponse = new CardResponse();


        cardResponse.setCardNumber(card.getCardNumber());
        int size = card.getTransactions().size();
        cardResponse.setTransactions(size >= 10 ?
                card.getTransactions().subList(size-10, size) : card.getTransactions());


        DecimalFormat df = new DecimalFormat("###,###.00");
        cardResponse.setAvailableAmount(df.format(card.getAvailableAmount()));
        return cardResponse;
    }

    public AuthorizationResponse setTransaction(Authorization auth) {
        return authorizationUC.process(auth);
    }

}
