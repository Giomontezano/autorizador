package br.com.hubfintech.autorizador.application.useCase;

import br.com.hubfintech.autorizador.adapter.database.TransactionRepository;
import br.com.hubfintech.autorizador.application.exception.InsulfficientCreditException;
import br.com.hubfintech.autorizador.application.exception.InvalidAccountException;
import br.com.hubfintech.autorizador.application.services.CardService;
import br.com.hubfintech.autorizador.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static java.text.NumberFormat.*;

@Service
public class AuthorizationUC {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private CardService cardService;

    public AuthorizationResponse process(Authorization auth) {
        Transaction transacao = generateTransactionReceived(auth);
        Card cardTransaction = null;

        try {
            //Consulta cartão
            cardTransaction = cardService.getCard(auth.getCardNumber());
            transacao.setCardNumber(cardTransaction);

            List<Transaction> transCard = cardTransaction.getTransactions();
            transCard.add(transacao);
            cardTransaction.setTransactions(transCard);

            //Valida saldo
            cardTransaction.setAvailableAmount(validateAmount(cardTransaction, transacao));

            transacao.setCardNumber(cardTransaction);
            transactionRepository.save(transacao);
            cardService.setCard(cardTransaction);

            return generateResponse(transacao, Code.APROVADO);
        } catch (InvalidAccountException e) {
            return generateResponse(transacao, Code.CONTA_INVALIDA);
        } catch (InsulfficientCreditException e) {
            return generateResponse(transacao, Code.SALDO_INSUFICIENTE);
        } catch (Exception e) {
            return generateResponse(transacao, Code.ERRO_PROCESSAMENTO);
        }
    }

    private Transaction generateTransactionReceived(Authorization auth) {
        Transaction transacao = new Transaction();
        transacao.setTransactionId(UUID.randomUUID());
        transacao.setTransactionType(auth.getAction());
        transacao.setDateTime(Date.from(Instant.now()));


        try {
            NumberFormat format = getInstance(Locale.FRANCE);
            transacao.setAmount(format.parse(auth.getAmount()).doubleValue());
        } catch(Exception e) {
            transacao.setAmount(null);
        }
        return transacao;
    }

    private Double validateAmount(Card cardTransaction, Transaction auth) {
        Double saldoAtual = (cardTransaction.getAvailableAmount() - auth.getAmount());

        if (saldoAtual < 0) {
            throw new InsulfficientCreditException();
        }
        return saldoAtual;
    }

    private AuthorizationResponse generateResponse(Transaction autorizacao, Code responseCode) {
        AuthorizationResponse response = new AuthorizationResponse();
        response.setAction(autorizacao.getTransactionType());
        response.setAuthorizationCode(autorizacao.getTransactionId().toString());
        response.setCode(responseCode);
        return response;
    }

}
